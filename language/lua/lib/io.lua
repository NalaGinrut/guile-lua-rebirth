io = {};

function io:close(fd)
   return __close(fd);
end

function io:flush(fd)
   return __flush(fd);
end

function io:input(fd)
   return __input(fd);
end

function io:lines(fd)
   return __lines(fd);
end

function io:open(file, mode)
   return __open(file, mode);
end

function io:output(fd)
   return __output(fd);
end

function io:popen(fd)
   return __popen(fd);
end

function io:read(fd)
   return __read(fd);
end

function io:tmpfile(prefix)
   return __tmpfile(prefix);
end

function io:type(obj)
   return __type(obj);
end

function io:write(obj)
   return __write(obj);
end
