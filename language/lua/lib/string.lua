string = {};

function string:format(fmt, ...)
   return __string_format(fmt, ...);
end

function string:sub(str, from, to)
   return __string_sub(str, from, to);
end

function string:char(ch)
   return __string_char(ch);
end

function string:find()
   return '';
end
