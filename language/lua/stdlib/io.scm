;;  Copyright (C) 2016,2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language lua stdlib io)
  #:use-module (language lua utils)
  #:use-module (language lua table)
  #:use-module (language lua scope)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:export (primitive:lua-print
            primitive:close
            primitive:flush
            primitive:input
            primitive:write))

;; NOTE: `print' returns nothing (unspecified in Guile). A variable assigned to
;;       `print' application result should get `nil' as its value.
(define (primitive:lua-print . args)
  (cond
   ((and (not (null? args)) (thunk? (car args)))
    (call-with-values
        (car args)
      (lambda (fmt al) (apply format #t fmt al))))
   (else (format #t "~{~a~^~/~}" (map return-fix args))))
  (newline))

(define (primitive:close fd)
  #t)

(define (primitive:flush fd)
  #t)

(define (primitive:input fd)
  #t)

(define (primitive:lines fd)
  #t)

(define (primitive:open fd)
  #t)

(define (primitive:doutputclose fd)
  #t)

(define (primitive:popen fd)
  #t)

(define (primitive:read fd)
  #t)

(define (primitive:tmpfile prefix)
  #t)

(define (primitive:type obj)
  #t)

(define (primitive:write obj)
  (write obj))

(define (lua-write args)
  (->stdlib-call 'io 'primitive:write args))

(define io-primitives
  `(("__write" . ,lua-write)))
