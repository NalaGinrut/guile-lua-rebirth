;;  Copyright (C) 2016,2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language lua stdlib misc)
  #:use-module (system base compile)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (language lua table)
  #:use-module (language lua irregex)
  #:use-module (language lua utils)
  #:export (primitive:lua-type
            primitive:setmetatable!
            primitive:getmetatable
            primitive:require))

;; NOTE: The type of type is string
(define (primitive:lua-type o)
  (match o
    ((? string?) "string")
    ((? number?) "number")
    ((? procedure?) "function")
    ((? lua-table?) "table")
    ('nil "nil")
    (else
     (glua-error
      'lua-detect-type
      "BUG: Invalid object `~a'!" o))))

(define (arg-assert obj check proc-name idx type)
  (when (not (check obj))
    (glua-error
     proc-name
     "Bad argument #~a to `~a' (~a expected, got ~a)"
     idx proc-name type (primitive:lua-type obj))))

(define (primitive:setmetatable! obj mt)
  (arg-assert obj lua-table? 'setmetatable 1 'table)
  (arg-assert obj lua-table? 'setmetatable 2 'table)
  (lua-table-meta-table! obj mt)
  obj)

(define (primitive:getmetatable obj)
  (arg-assert obj lua-table? 'getmetatable 1 'table)
  (lua-table-meta-table obj))

(define (import-from-scm-module mod-name)
  (define (rename s)
    (string->symbol ((compose -->_ :->_ *->_ !->_x) (symbol->string s))))
  (define (redefine k v)
    (when (variable-bound? v)
      (module-define! (current-module) (rename k) (variable-ref v))))
  (catch #t
    (lambda ()
      (let ((mod (resolve-module mod-name)))
        (cond
         ((module-filename mod)
          (module-for-each redefine mod)
          (hash-for-each redefine (module-import-obarray mod))
          #t)
         (else #f))))
    (lambda _ #f)))

(define *lua-ext-re* (string->irregex "([^.]+).lua"))
(define* (primitive:require mod/filename #:optional (lang "lua"))
  (define m (irregex-match *lua-ext-re* mod/filename))
  (define filename
    (if m
        mod/filename
        (string-append mod/filename ".lua")))
  (define (gen-mod-name)
    (map string->symbol
         (string-split
          (if m (irregex-match-substring m 1) mod/filename)
          #\/)))
  (define (require f)
    (parameterize ((need-preload? #t))
      (compile-and-load f #:from 'lua)
      #t))
  (define (search-lib-path)
    (or (let ((f (format #f "~a/~a" (getcwd) filename)))
          (and (file-exists? f) f))
        (and=> (%search-load-path "language/lua/spec")
               (lambda (p)
                 (format #f "~a/lib/~a"  (dirname p) filename)))))
  (cond
   ((string=? lang "scm") (import-from-scm-module (gen-mod-name)))
   ((search-lib-path) => require)
   (else (require filename))))
