Guile-Lua (gLua)
=========

gLua is a Lua frontend on Guile platform. Guile is the GNU Ubiquitous Intelligent Language
for Extensions, and the official extension language of the GNU project.

Guile-Lua aims to be compatible with Lua-5.2, and intractive with other Guile programes writtern in
other languages on Guile. The first target is to work with [GNU Artanis](https://artanis.dev), actually, it works now.

Guile-Lua is still working in progress, but almost done and works fine.
Patches and communications are welcome!

This work was presented in [ICFP 2019 Scheme Workshop](https://icfp19.sigplan.org/track/scheme-2019-papers).

[Techinical report](https://gitlab.com/NalaGinrut/guile-lua-rebirth/blob/lua5.2-origin/glua2019.pdf)

[Present Slide](https://gitlab.com/NalaGinrut/guile-lua-rebirth/blob/lua5.2-origin/icfp2019-sw-slide.pdf)

[Present Video](https://youtu.be/Gd5Lq7RUxhQ)
